#!/usr/bin/env python

import sys
import multiprocessing as mp

def map_func(line):
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into fields
    passenger_id, _, _, _, _, _ = line.split(',')
    # emit (passenger_id, 1) key-value pair
    return (passenger_id, 1)

if __name__ == '__main__':
    # input comes from STDIN (standard input)
    with sys.stdin as f:
        map_in = f.readlines()
        with mp.Pool(processes=mp.cpu_count()) as pool:
            map_out = pool.map(map_func, map_in, chunksize=int(len(map_in)/mp.cpu_count()))
            for key, value in map_out:
                # write the results to STDOUT
                print('%s\t%s' % (key, value))
