#!/usr/bin/env python

import sys
import multiprocessing as mp
from mapper import map_func

def reduce_job(input_list):
    # initialize variables for tracking passenger with highest number of flights
    current_passenger = None
    current_count = 0
    passenger = None
    max_count = 0
    max_passenger = None

    # iterate through each line of input from mapper
    for line in input_list:
        # Remove leading and trailing whitespace
        line = line.strip()

        # Parse the input retrieved from mapper
        key, value = line.split('\t', 1)

        # Count total number of flights for each passenger
        if key == current_passenger:
            current_count += int(value)
        else:
            if current_passenger:
                # write passenger's flight count to STDOUT
                print('%s\t%s' % (current_passenger, current_count))
                # update max count and passenger if necessary
                if current_count > max_count:
                    max_count = current_count
                    max_passenger = current_passenger
            # reset current count and passenger to new passenger
            current_count = int(value)
            current_passenger = key

    # output last passenger's flight count if needed
    if current_passenger == passenger:
        print('%s\t%s' % (current_passenger, current_count))
        if current_count > max_count:
            max_count = current_count
            max_passenger = current_passenger

    # output the passenger with highest number of flights
    print('\nPassenger with the highest number of flights: %s' % max_passenger)
    print('Number of flights: %s' % max_count)

if __name__ == '__main__':
    # input comes from STDIN (standard input)
    reduce_in = sys.stdin.readlines()

    # use multiprocessing pool to run reduce_job function on input
    with mp.Pool(processes=mp.cpu_count()) as pool:
        pool.map(reduce_job, [reduce_in], chunksize=int(len(reduce_in)/mp.cpu_count()))
